<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = new User();
        $orders = new Orders();
        $userId = Auth::user()->id;

        return view('user', ['user' => $user->find($userId),
            'orders' => $orders->all()
        ]);
    }

    public function address()
    {
        $orders = new Orders();
        $user = new User();
        $userId = Auth::user()->id;

        return view('address', [
            'user' => $user->find($userId),
            'orders' => $orders->all()
        ]);

    }

    public function change_address(Request $request)
    {
        $orders = new Orders();
        $user = new User();
        $userId = Auth::user()->id;

        DB::table('users')
            ->where('id', '=', $user->find($userId)->id)
            ->update(['address' => $request->address]);

        return view('admin', ['orders' => $orders->all()]);
    }

    public  function toorders() {
        $orders = new Orders();
        if (Auth::check()) {
            $userId = Auth::user()->id;

//            unset($_COOKIE['lk']);
//            setcookie('lk', null, -1, '/');

            $i = 1;
            while (isset($_COOKIE['name' . $i])) {
                DB::table('orders')->insert([
                    [
                        'name_device' => $_COOKIE['name' . $i],
                        'id_user'=>$userId
                    ]
                ]);

                unset($_COOKIE['name' . $i]);
                setcookie('name' . $i, null, -1, '/');

                $i++;
            }
        }

        return view('admin', ['orders' => $orders->all()]);
    }

    public function admin()
    {
        $orders = new Orders();

        if (Auth::check()) {
            $user = new User();
            $userId = Auth::user()->id;
            $userRole = $user->find($userId)->role;
            if ($userRole == 1)
                return view('admin', ['orders' => $orders->all()]);
            else
                return response()->redirectTo('/');
        }
    }
}
