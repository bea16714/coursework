<?php

namespace App\Http\Controllers;

use App\Models\Desktop;
use App\Models\Notebooks;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Models\Details;

class MainController extends Controller {

    public function welcome() {
        $slider = new Slider();
        return view('welcome', ['slider' => $slider->all()]);
    }

    public function buyers() {
        return view('buyers');
    }

    public function owners() {
        return view('owners');
    }

    public function about() {
        return view('about');
    }

    public function page_in_progress() {
        return view('page_in_progress');
    }

    public function admin() {
        return view('admin');
    }

    public function toorders() {

        //return view('basket');
    }

    public function basket() {
        $details = new Details();
        $price = [];
        foreach ($details->all() as $el) {
            $i = 1;
            while (isset($_COOKIE['name' . $i])) {
                if ($el->name == $_COOKIE['name' . $i]) {
                    array_push($price, $el->price);
                }
                $i++;
            }
        }
        return view('basket', ['price' => $price]);
    }

    public function notebooks($name = '') {
        $notebooks = new Notebooks();
        $details = new Details();
        if ($name == '')
            return view('notebooks', ['notebooks' => $notebooks->all()]);
        else
        {
            $detail = $details->all();

            $success = 0;

            foreach ($detail as $el) {
                if ($name == strtolower(str_replace(" ", '', $el['name']))) {
                    $success = 1;
                    $id = $el['id'];
                    break;
                }
            }
            if ($success == 0) {
                abort(404);
            }
            else {
                return view('detail', ['details' => $details->find($id)]);
            }
        }

    }

    public function pc($name = '') {
        $desktop = new Desktop();
        $details = new Details();

        if ($name == '')
            return view('pc', ['desktop' => $desktop->all()]);
        else
        {
            $detail = $details->all();

            $success = 0;

            foreach ($detail as $el) {
                if ($name == strtolower(str_replace(" ", '', $el['name']))) {
                    $success = 1;
                    $id = $el['id'];
                    break;

                }
            }
            if ($success == 0) {
                abort(404);
            }
            else {
                return view('detail', ['details' => $details->find($id)]);
            }
        }
    }

    public function computers($tags = '', $value = '') {
        $notebooks = new Notebooks();
        $desktop = new Desktop();
        $details = new Details();

        $n = $notebooks->all();
        $d = $desktop->all();

        if ($tags == '') {
            $i = 0;
            foreach ($n as $el) {
                $union[$i]['name'] = $el->name;
                $union[$i]['img'] = $el->img;
                $union[$i]['price'] = $el->price;
                $i++;
            }

            foreach ($d as $el) {
                $union[$i]['name'] = $el->name;
                $union[$i]['img'] = $el->img;
                $union[$i]['price'] = $el->price;
                $i++;
            }
            return view('computers', ['notebooks' => $union]);
        }
        else {
            $union = '';
            switch ($tags) {
                case 'price':
                    $value = substr($value, 1);
                    $union = $this->searchPrice($value);
                    break;

                case 'gpu':
                    $value = substr($value, 3);
                    $union = $this->searchGpu($value);
                    break;

                case 'cpu':
                    $value = substr($value, 3);
                    $union = $this->searchCpu($value);
                    break;
            }
//            if ($union == '')
//                abort(404);
//            else
//                return view('computers', ['notebooks' => $union]);
        }



    }

    function searchPrice($value)
    {
        $notebooks = new Notebooks();
        $desktop = new Desktop();

        $n = $notebooks->all();
        $d = $desktop->all();

        $value = $value . '000';

        $i = 0;
        foreach ($n as $el) {
            $price = str_replace(" ", '', $el->price);
            $price = str_replace("₽", '', $price);
            if ($value >= $price) {
                $union[$i]['name'] = $el->name;
                $union[$i]['img'] = $el->img;
                $union[$i]['price'] = $el->price;
                $i++;
            }
        }

        foreach ($d as $el) {
            $price = str_replace(" ", '', $el->price);
            $price = str_replace("₽", '', $price);

            if ($value >= $price) {
                $union[$i]['name'] = $el->name;
                $union[$i]['img'] = $el->img;
                $union[$i]['price'] = $el->price;
                $i++;
            }
        }
        if (isset($union))
            return $union;
    }

    function searchGpu($value)
    {
        $notebooks = new Notebooks();
        $desktop = new Desktop();
        $details = new Details();

        $n = $notebooks->all();
        $des = $desktop->all();
        $det = $details->all();

        $value = "NVIDIA GeForce RTX 30" . $value . "0";
        $i = 0;

        foreach ($det as $el2) {
            if ($el2->gpu == $value) {
                $id = $el2->id;

                foreach ($n as $el) {
                    if ($el->details_id == $id) {
                        $union[$i]['name'] = $el->name;
                        $union[$i]['img'] = $el->img;
                        $union[$i]['price'] = $el->price;
                        $i++;
                    }
                }

                foreach ($des as $el) {
                    if ($el->details_id == $id) {
                        $union[$i]['name'] = $el->name;
                        $union[$i]['img'] = $el->img;
                        $union[$i]['price'] = $el->price;
                        $i++;
                    }
                }
            }
        }

        return $union;
    }

    function searchCpu($value) {
        $notebooks = new Notebooks();
        $desktop = new Desktop();
        $details = new Details();

        $n = $notebooks->all();
        $des = $desktop->all();
        $det = $details->all();

        if ($value[0] == 'i') {
            $value = "Intel Core i" . $value[1];
        }

        if ($value[0] == 'r') {
            $value = "AMD Ryzen " . $value[1];
        }

        echo $value[0];
        $i = 0;
    }

}
