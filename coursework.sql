-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 21 2022 г., 12:48
-- Версия сервера: 8.0.24
-- Версия PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `coursework`
--

-- --------------------------------------------------------

--
-- Структура таблицы `desktops`
--

CREATE TABLE `desktops` (
  `id` bigint UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cooling` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `storage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guarantee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `desktops`
--

INSERT INTO `desktops` (`id`, `img`, `name`, `description`, `price`, `customization`, `cooling`, `storage`, `guarantee`, `details_id`, `created_at`, `updated_at`) VALUES
(1, 'cyber.jpg', 'CYBER', 'Эталон современных компьютеров, в которых всё продумано до мелочей. Стиль и быстродействие в неподражаемом корпусе для игры и работы.', '200 000 ₽', 'Индивидуальная кастомизация', 'Кастомное водяное охлаждение', 'До 30 ТБ ', 'Удвоенная гарантия', 3, NULL, NULL),
(2, 'enthusiast.jpg', 'ENTHUSIAST', 'Мощные игровые компьютеры с безграничными возможностями апгрейда. Неординарный и дерзкий, предназначен для решения задачи любой сложности.', '300 000 ₽', 'Без кастомизации', 'Стандартное водяное охлаждение', 'До 60 ТБ ', 'Стандартная гарантия', 4, NULL, NULL),
(3, 'concept.jpg', 'CONCEPT', 'Исключительная ручная сборка - как новый вид современного искусства. Передовые технологии и уникальный дизайн для истинных ценителей красоты.', '400 000 ₽', 'Индивидуальная кастомизация', 'Кастомное водяное охлаждение', 'До 60 ТБ', 'Удвоенная гарантия', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `details`
--

CREATE TABLE `details` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_h1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_h1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thickness` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_h1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_pubg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_warzone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance_cs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gpu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resolution` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modeling` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_editing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visualization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `design` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streaming` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `details`
--

INSERT INTO `details` (`id`, `name`, `tagline`, `img1`, `description_h1`, `description_text`, `design_h1`, `design_description`, `length`, `width`, `thickness`, `weight`, `design_img`, `performance_h1`, `performance_description`, `performance_img`, `performance_pubg`, `performance_warzone`, `performance_cs`, `price`, `cpu`, `gpu`, `resolution`, `modeling`, `video_editing`, `visualization`, `design`, `streaming`, `created_at`, `updated_at`) VALUES
(1, 'PLAY 15', 'ВОЗЬМИ ИГРУ С СОБОЙ', 'bg1.webp', 'ЗАРЯЖЕН НОВЕЙШИМИ ТЕХНОЛОГИЯМ', 'Мобильность превыше всего. Ноутбук PLAY 15 представляет собой ультратонкую игровую платформу на базе операционной системы Windows 10. Легкий и прочный лэптоп готов к серьезным игровым победам. Смелый дизайн изящного корпуса, толщиной всего 26 мм, скрывает в себе мощь графики GeForce RTX 30 серии и процессоров Intel Core i7 или AMD Ryzen 7. Роскошный дисплей с частотой до 165 Гц подарит незабываемые ощущения игрового процесса.', 'СТРОГИЕ ФОРМЫ. ХИЩНЫЙ СТИЛЬ.', 'Ноутбук выполнен в тонком (26 мм) корпусе с лаконичным и строгим дизайном. Однако геймерская натура устройства угадывается по RGB полосе и безрамочному дисплею, что придает ноутбуку утонченности и позволяет лучше концентрироваться на изображении. Корпус выполнен из алюминия и обладает комфортным весом всего в 2,2 кг что является отличным показателем для мощного игрового устройства в таком компактном кейсе.', '360 мм', '243 мм', '26 мм', '2,2 кг', 'bg2.webp', 'ДЛЯ ИГР, РАБОТЫ И ВСЕГО ОСТАЛЬНОГО', 'PLAY 15 отвечает самым высоким требованиям игрового ноутбука, оптимизированной до мельчайших деталей и рассчитанной на максимальную производительность. Процессоры Intel Core i7 или AMD Ryzen 7 в связке с видеокартами NVIDIA Geforce RTX 30-серии в дизайне MAX-Q, являются не только самыми быстрыми компонентами для ноутбуков доступные в настоящее время, но и гарантируют плавный игровой процесс с максимальными настройками графики.', 'bg3.webp', '132 FPS', '140 FPS', '260 FPS', '100 000 ₽', 'AMD Ryzen 7 5800H', 'NVIDIA GeForce RTX 3060', '2560 x 1440px', '0', '0', '0', '0', '0', NULL, NULL),
(2, 'PLAY 17', 'КОМПАКТНЫЙ. ЛЁГКИЙ. МОЩНЫЙ. НАДЁЖНЫЙ', 'bg1.jpg', 'СОБРАН ДЛЯ ПОБЕД', 'Ноутбук PLAY 17 – это ультратонкая и мощная игровая станция на базе операционной системы Windows 10. Под небольшим изящным корпусом сокрыта вся мощь игровых мобильных видеокарт GeForce RTX 30-серии и процессора Intel Core i7-11800H. Продуманная система охлаждения с несколькими режимами работы. Современный QHD-дисплей с частотой дисплея 165 Гц всегда показывает самое важное максимально ярко и красиво.', 'СТРОГИЙ ВНЕШНИЙ ВИД, НО СВОЙ СТИЛЬ', 'Ноутбук имеет лаконичный и строгий дизайн, выполненный из очень тонкого корпуса в 20 мм. Но не обманывайтесь – он всё равно предназначен для геймеров, а яркая RGB-полоса и дисплей без рамок не дают отвлекаться от происходящего на экране. Корпус выполнен из сплава магния и весит всего 2,2 кг. Ещё никогда мощные игровые устройства не были такими компактными.', '394 мм', '260 мм', '20 мм', '2,3 кг', 'bg2.jpg', 'ИГРЫ, РАЗВЛЕЧЕНИЯ И РАБОТА НА МАКСИМУМЕ', 'PLAY предназначен для качественного гейминга, поэтому он способен давать максимальную производительность. Связка из видеокарты NVIDIA GeForce RTX 30-серии и процессора Intel Core i7-11800H обеспечивают пользователю максимально плавный геймплей с красивой картинкой.', 'bg3.jpg', '132 FPS', '140 FPS', '260 FPS', '200 000 ₽', 'AMD Ryzen 9 5900HX', 'NVIDIA GeForce RTX 3070', '2560 x 1440px', '0', '0', '0', '0', '0', NULL, NULL),
(3, 'CYBER', 'МОЩНЫЙ. НАДЁЖНЫЙ', 'bg1.jpg', 'ЗАРЯЖЕН НОВЕЙШИМИ ТЕХНОЛОГИЯМ', 'Эталон современных компьютеров, в которых всё продумано до мелочей. Стиль и быстродействие в неподражаемом корпусе для игры и работы.', 'Дерзкий темперамент.', 'Мощный и роскошный. Дерзкий и элегантный. Утонченный и решительный. Компьютер HYPERPC CYBER сочетает в себе контрасты и адресован тем, кто ищет для себя нечто особенное. За ярким обликом этого роскошного компьютера, скрывается стремительный характер и качества, присущие истинному hi-end решению HYPERPC.', '42 ', '42', '42', '42', 'bg2.jpg', 'Хищная мощь и впечатляющая производительность.', 'Высокая производительность системы обеспечивает ошеломляющий уровень комфорта в самых требовательных современных играх. Запаса мощности HYPERPC CYBER с лихвой хватит на долгожданные игры ближайших лет. Компьютер укомплектован графическими адаптерами NVIDIA GeForce RTX, что гарантирует яркий спектр впечатлений и полное погружение в игровой процесс.', 'bg3.jpg', '100 FPS', '100 FPS', '100 FPS', '200 000 P', 'Intel Core i5-10400', 'NVIDIA GeForce RTX 3080', '2560 x 1440px', '1', '1', '1', '1', '1', NULL, NULL),
(4, 'ENTHUSIAST', 'МОЩНЫЙ. НАДЁЖНЫЙ', 'bg1.jpg', 'ЗАРЯЖЕН НОВЕЙШИМИ ТЕХНОЛОГИЯМ', 'Эталон современных компьютеров, в которых всё продумано до мелочей. Стиль и быстродействие в неподражаемом корпусе для игры и работы.', 'Дерзкий темперамент.', 'Мощный и роскошный. Дерзкий и элегантный. Утонченный и решительный. Компьютер HYPERPC CYBER сочетает в себе контрасты и адресован тем, кто ищет для себя нечто особенное. За ярким обликом этого роскошного компьютера, скрывается стремительный характер и качества, присущие истинному hi-end решению HYPERPC.', '42 ', '42', '42', '42', 'bg2.jpg', 'Хищная мощь и впечатляющая производительность.', 'Высокая производительность системы обеспечивает ошеломляющий уровень комфорта в самых требовательных современных играх. Запаса мощности HYPERPC CYBER с лихвой хватит на долгожданные игры ближайших лет. Компьютер укомплектован графическими адаптерами NVIDIA GeForce RTX, что гарантирует яркий спектр впечатлений и полное погружение в игровой процесс.', 'bg3.jpg', '200 FPS', '200 FPS', '200 FPS', '300 000 P', 'Intel Core i7-10700', 'NVIDIA GeForce RTX 3090', '2560 x 1440px', '1', '1', '1', '1', '1', NULL, NULL),
(5, 'CONCEPT', 'МОЩНЫЙ. НАДЁЖНЫЙ', 'bg1.jpg', 'ЗАРЯЖЕН НОВЕЙШИМИ ТЕХНОЛОГИЯМ', 'Эталон современных компьютеров, в которых всё продумано до мелочей. Стиль и быстродействие в неподражаемом корпусе для игры и работы.', 'Дерзкий темперамент.', 'Мощный и роскошный. Дерзкий и элегантный. Утонченный и решительный. Компьютер HYPERPC CYBER сочетает в себе контрасты и адресован тем, кто ищет для себя нечто особенное. За ярким обликом этого роскошного компьютера, скрывается стремительный характер и качества, присущие истинному hi-end решению HYPERPC.', '42 ', '42', '42', '42', 'bg2.jpg', 'Хищная мощь и впечатляющая производительность.', 'Высокая производительность системы обеспечивает ошеломляющий уровень комфорта в самых требовательных современных играх. Запаса мощности HYPERPC CYBER с лихвой хватит на долгожданные игры ближайших лет. Компьютер укомплектован графическими адаптерами NVIDIA GeForce RTX, что гарантирует яркий спектр впечатлений и полное погружение в игровой процесс.', 'bg3.jpg', '300 FPS', '300 FPS', '300 FPS', '400 000 P', 'Intel Core i9-10900', 'NVIDIA GeForce RTX 3090', '2560 x 1440px', '1', '1', '1', '1', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_12_192847_create_desktops_table', 1),
(6, '2022_01_12_192943_create_details_table', 1),
(7, '2022_01_12_193017_create_notebooks_table', 1),
(8, '2022_01_12_193050_create_sliders_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `notebooks`
--

CREATE TABLE `notebooks` (
  `id` bigint UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `availability` tinyint(1) NOT NULL,
  `keyboard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thickness` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `notebooks`
--

INSERT INTO `notebooks` (`id`, `img`, `name`, `price`, `availability`, `keyboard`, `thickness`, `weight`, `details_id`, `created_at`, `updated_at`) VALUES
(1, 'play15.jpg', 'PLAY 15', '100 000 ₽', 1, 'Механическая', '26 мм', '2.2 кг', 1, NULL, NULL),
(2, 'play17.jpg', 'PLAY 17', '200 000 ₽', 0, 'Мембранная', '22 мм', '2.3 кг', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL,
  `name_device` varchar(255) NOT NULL,
  `id_user` int NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `name_device`, `id_user`, `updated_at`, `created_at`) VALUES
(1, 'PLAY 15', 1, NULL, NULL),
(2, 'PLAY 17', 1, NULL, NULL),
(3, 'PLAY 15', 1, NULL, NULL),
(4, 'PLAY 15', 1, NULL, NULL),
(5, 'PLAY 15', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `big_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sliders`
--

INSERT INTO `sliders` (`id`, `img`, `big_text`, `small_text`, `created_at`, `updated_at`) VALUES
(1, 'intel.jpg', 'ПРОЦЕССОРЫ INTEL CORE 12-ГО ПОКОЛЕНИЯ ДЛЯ ПК HYPERPC', 'СОЗДАНЫ ДЛЯ НОВОГО ПОКОЛЕНИЯ ИГР', NULL, NULL),
(2, 'msi.jpg', 'КУПИ HYPERPC С ПЕРИФЕРИЕЙ ОТ MSI', 'И ПОЛУЧИ ДО 8000 РУБ НА АККАУНТ STEAM В ПОДАРОК', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role`, `address`, `created_at`, `updated_at`) VALUES
(1, '123', '123@mail.ru', NULL, '$2y$10$txKdF0c2j/XD/oJSAERvmur4rqithWJgFPD7ti9s6jqX1TplOd2jO', NULL, 1, '123', '2022-01-20 14:53:13', '2022-01-20 14:53:13'),
(2, '123', '321@maim.ru', NULL, '$2y$10$DQM5f6y4orew0Q950DD8Cu3/M.g5MPvAEEYHNeHwdIse/P/P49wnG', NULL, NULL, NULL, '2022-01-20 16:27:35', '2022-01-20 16:27:35'),
(3, 'test', 'test@test.ru', NULL, '$2y$10$WVSq8sTzRDlrbmEvRYEQgOwh.ovQXlBZFmaDEF.TCt7qXmVdJhsqq', NULL, NULL, NULL, '2022-01-20 16:35:33', '2022-01-20 16:35:33');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `desktops`
--
ALTER TABLE `desktops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notebooks`
--
ALTER TABLE `notebooks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `desktops`
--
ALTER TABLE `desktops`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `details`
--
ALTER TABLE `details`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `notebooks`
--
ALTER TABLE `notebooks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
