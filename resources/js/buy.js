/* IN GARBAGE */
const name = document.getElementById("buy-name").innerHTML;
const img = document.getElementById("card_img").src;

document.getElementById('in_garbage').onclick = function () {

    let i = 1;

    if (getCookie('name1') != undefined) {
        while (getCookie('name' + i) != undefined) {
            i++;
        }
    }

    $.cookie('name' + i, name, { expires: 7, path: '/' });
    $.cookie('img' + i, img, { expires: 7, path: '/' });

    const newCookies = {};
    newCookies[0] = [name, img];

    dropdownItems.append(...getList(newCookies));

}

document.getElementById('clearBasket').onclick = function () {

    let i = 1;

    if (getCookie('name1') != undefined) {
        while (getCookie('name' + i) != undefined) {
            $.removeCookie('name' + i);
            $.removeCookie('img' + i);
            i++;
        }
    }

}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function getList(newCookies) {
    let result = [];

    result.push(getDropdown(newCookies[0][1], newCookies[0][0]));

    return result;

}

function getDropdown(imgPatch, name) {
    let dropdownItem = document.createElement('div');
    let dropdownItemImg = document.createElement('div');
    let dropdownItemName = document.createElement('div');
    let img = document.createElement('img');
    img.src = imgPatch;


    dropdownItem.className = 'dropdown-item';
    dropdownItemImg.className = 'dropdown-item-img';
    dropdownItemName.className = 'dropdown-item-name';


    dropdownItemImg.append(img);
    dropdownItemName.append(name);
    dropdownItem.append(dropdownItemImg);
    dropdownItem.append(dropdownItemName);

    return dropdownItem;
}
