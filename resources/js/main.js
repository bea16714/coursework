/* SELECT */
const parameter = document.getElementById('by-parameter');
const games = document.getElementById('for-games');
const work = document.getElementById('for-work');

const parameters = document.getElementById('parameters');
const resolution = document.getElementById('resolution');
const works = document.getElementById('work');

function allEnable1() {
    parameter.classList.remove("enable");
    games.classList.remove("enable");
    work.classList.remove("enable");

    allEnable2();
    allEnable3();
    allEnable4();
    allEnable5();

    down.classList.add("none");

    price2.classList.add("none");
    gpu2.classList.add("none");
    cpu2.classList.add("none");

    down2.classList.add("none");
}

function allNone1() {
    parameters.classList.add('none');
    resolution.classList.add('none');
    works.classList.add('none');
}

parameter.onclick = function() {
    allEnable1();
    parameter.classList.add("enable");

    allNone1();
    parameters.classList.remove('none');
};

games.onclick = function() {
    allEnable1();
    games.classList.add("enable");

    allNone1();
    resolution.classList.remove('none');
};

work.onclick = function() {
    allEnable1();
    work.classList.add("enable");

    allNone1();
    works.classList.remove('none');
};

const price = document.getElementById('price');
const gpu = document.getElementById('gpu');
const cpu = document.getElementById('cpu');

const fullhd = document.getElementById('fullhd');
const k2 = document.getElementById('k2');
const k4 = document.getElementById('k4');

const modeling = document.getElementById('modeling');
const videoediting = document.getElementById('videoediting');
const visualization = document.getElementById('visualization');
const design = document.getElementById('design');
const streaming = document.getElementById('streaming');

const down = document.getElementById('down');

const price2 = document.getElementById('price2');
const gpu2 = document.getElementById('gpu2');
const cpu2 = document.getElementById('cpu2');

function allEnable2() {
    price.classList.remove("enable");
    gpu.classList.remove("enable");
    cpu.classList.remove("enable");

    down.classList.remove("none");
}

function allNone2() {
    price2.classList.add('none');
    gpu2.classList.add('none');
    cpu2.classList.add('none');
}

price.onclick = function() {
    allEnable2();
    price.classList.add("enable");

    allNone2();
    price2.classList.remove('none');
}

gpu.onclick = function() {
    allEnable2();
    gpu.classList.add("enable");

    allNone2();
    gpu2.classList.remove('none');
}

cpu.onclick = function() {
    allEnable2();
    cpu.classList.add("enable");

    allNone2();
    cpu2.classList.remove('none');
}

function allEnable3() {
    fullhd.classList.remove("enable");
    k2.classList.remove("enable");
    k4.classList.remove("enable");

    down.classList.remove("none");
}

fullhd.onclick = function() {
    allEnable3();
    fullhd.classList.add("enable");
}

k2.onclick = function() {
    allEnable3();
    k2.classList.add("enable");
}

k4.onclick = function() {
    allEnable3();
    k4.classList.add("enable");
}

function allEnable4() {
    modeling.classList.remove("enable");
    videoediting.classList.remove("enable");
    visualization.classList.remove("enable");
    design.classList.remove("enable");
    streaming.classList.remove("enable");

    down.classList.remove("none");
}

modeling.onclick = function() {
    allEnable4();
    modeling.classList.add("enable");
}
videoediting.onclick = function() {
    allEnable4();
    videoediting.classList.add("enable");
}
visualization.onclick = function() {
    allEnable4();
    visualization.classList.add("enable");
}
design.onclick = function() {
    allEnable4();
    design.classList.add("enable");
}
streaming.onclick = function() {
    allEnable4();
    streaming.classList.add("enable");
}

const p100 = document.getElementById('p100');
const p200 = document.getElementById('p200');
const p300 = document.getElementById('p300');
const p400 = document.getElementById('p400');

const gpu6 = document.getElementById('gpu6');
const gpu7 = document.getElementById('gpu7');
const gpu8 = document.getElementById('gpu8');
const gpu9 = document.getElementById('gpu9');

const cpui5 = document.getElementById('cpui5');
const cpui7 = document.getElementById('cpui7');
const cpui9 = document.getElementById('cpui9');
const cpur5 = document.getElementById('cpur5');
const cpur7 = document.getElementById('cpur7');
const cpur9 = document.getElementById('cpur9');


const down2 = document.getElementById('down2');


let arrPrice = [p100, p200, p300, p400];
let arrGpu = [gpu6, gpu7, gpu8, gpu9];
let arrCpu = [cpui5, cpui7, cpui9, cpur5, cpur7, cpur9];
let arrWork = [modeling, videoediting, visualization, design, streaming];

let arr = [p100, p200, p300, p400, gpu6, gpu7, gpu8, gpu9, cpui5, cpui7, cpui9, cpur5, cpur7, cpur9, modeling, videoediting, visualization, design, streaming]

function allEnable5() {
    arr.forEach(function(item) {
        item.classList.remove("enable");
    });

}

arrPrice.forEach(function(item) {

    item.onclick = function() {
        allEnable5();
        item.classList.add("enable");
        window.location.href = "computers/price/" + item.id;
    }
});

arrGpu.forEach(function(item) {

    item.onclick = function() {
        allEnable5();
        item.classList.add("enable");
        window.location.href = "computers/gpu/"  + item.id;
    }
});

arrCpu.forEach(function(item) {

    item.onclick = function() {
        allEnable5();
        item.classList.add("enable");
        window.location.href = "computers/cpu/" + item.id;
    }
});

arrWork.forEach(function(item) {

    item.onclick = function() {
        allEnable5();
        item.classList.add("enable");
        window.location.href = "computers/work/" + item.id;
    }
});
