//alert( document.cookie );
setTimeout(getItems, 100);

function getItems() {
    if ($.cookie("name1") != null) {
        let i = 1;
        const cookies = {};
        while ($.cookie("name" + i) != null) {

            cookies[i-1] = [$.cookie("name" + i), $.cookie("img" + i)];

            i++;
        }

        dropdownItems.append(...getListHeader(cookies));

    }

}


function getListHeader(cookies) {
    let result = [];

    for (let i=0; i!=Object.keys(cookies).length; i++) {
        let dropdownItem = document.createElement('div');
        let dropdownItemImg = document.createElement('div');
        let dropdownItemName = document.createElement('div');
        let img = document.createElement('img');
        img.src = cookies[i][1];


        dropdownItem.className = 'dropdown-item';
        dropdownItemImg.className = 'dropdown-item-img';
        dropdownItemName.className = 'dropdown-item-name';


        dropdownItemImg.append(img);
        dropdownItemName.append(cookies[i][0]);
        dropdownItem.append(dropdownItemImg);
        dropdownItem.append(dropdownItemName);

        result.push(dropdownItem);

    }

    return result;

}

function getBasket() {
    let result = [];

    let i = 1;
    while ($.cookie("name" + i) != null) {
        let priceText = 100000;

        let dropdownItem = document.createElement('div');
        let dropdownItemImg = document.createElement('div');
        let dropdownItemName = document.createElement('div');
        let price = document.createElement('div');
        let img = document.createElement('img');
        img.src = $.cookie("img" + i);


        dropdownItem.className = 'dropdown-item';
        dropdownItemImg.className = 'dropdown-item-img';
        dropdownItemName.className = 'dropdown-item-name';
        price.className = 'basket-page-price';


        dropdownItemImg.append(img);
        dropdownItemName.append($.cookie("name" + i));
        price.append(priceText);
        dropdownItem.append(dropdownItemImg);
        dropdownItem.append(dropdownItemName);
        dropdownItem.append(price);

        result.push(dropdownItem);

        i++;

    }

    return result;
}
