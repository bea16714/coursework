@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="about-page container">
        <h1 class="about-h1 text-center text-white">Мир EDEVICE</h1>
        <div class="row">
            <a href="/coursework/public/page_in_progress" class="about-link col-xl-4 col-md-6">
                <div class="about-card">
                    <img class="about-img" src="/coursework/public/img/about/about1.jpg" alt="about1">
                    <div class="about-down">
                        <div class="about-title text-white">Контакты</div>
                        <div class="about-description">Ответим на все интересующие вопросы</div>
                        <div class="about-text">Мы с радостью поможем вам с выбором компьютера и ответим на все интересующие вопросы!</div>
                    </div>

                </div>
            </a>
            <a href="/coursework/public/page_in_progress" class="about-link col-xl-4 col-md-6">
                <div class="about-card">
                    <img class="about-img" src="/coursework/public/img/about/about2.jpg" alt="about1">
                    <div class="about-down">
                        <div class="about-title text-white">О нас</div>
                        <div class="about-description">Познакомься с историей бренда</div>
                        <div class="about-text">Основанная в 2021 году, компания EDevice не является российским лидером отрасли, производящей компьютеры премиум класса. Мы не ставим перед собой цель — стать наиболее уважаемым брендом в мире гейминга и киберспорта.</div>
                    </div>

                </div>
            </a>
            <a href="/coursework/public/page_in_progress" class="about-link col-xl-4 col-md-6">
                <div class="about-card">
                    <img class="about-img" src="/coursework/public/img/about/about3.jpg" alt="about1">
                    <div class="about-down">
                        <div class="about-title text-white">Работа в компании</div>
                        <div class="about-description">Постройте карьеру в EDevice</div>
                        <div class="about-text">Мы — безумные фанаты мощных и красивых компьютеров, топового железа и игр. Нам нужны сотрудники, которые разделяют наши интересы, с детства любят компьютеры и компьютерные игры, интересуются новыми технологиями и хотят стать частью нашей дружной команды.</div>
                    </div>

                </div>
            </a>
        </div>
    </div>

@endsection
