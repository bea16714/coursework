@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="buyers container">
        <h1 class="buyers-h1 text-center text-white">Владельцам</h1>
        <div class="buyers-text">
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque eius expedita illum magni
                minus, molestias natus nemo nihil, quae quas quia quisquam recusandae. A facilis minus numquam quidem
                veritatis!
            </div>
            <div>At blanditiis cum dicta eum exercitationem fugit modi mollitia omnis repellat suscipit! Blanditiis
                consequuntur, eius enim molestiae mollitia obcaecati odio, officiis omnis quaerat quod recusandae
                repellendus reprehenderit repudiandae sit vitae.
            </div>
            <div>Dignissimos earum hic ipsum quas repudiandae sequi ut vitae! Cumque cupiditate doloremque earum,
                expedita ipsam perferendis praesentium quis unde voluptatum. Atque beatae consequuntur, laudantium optio
                possimus quibusdam ratione vel voluptatum.
            </div>
            <div>Ab accusantium adipisci aperiam aspernatur atque, debitis dolorem error esse et expedita fugit laborum
                maxime nam nesciunt odio perspiciatis quisquam quo reiciendis repellat, reprehenderit sit ullam vel
                vitae voluptas voluptatibus!
            </div>
            <div>Accusamus aspernatur aut illum ipsam modi quos sequi. Commodi dolorem eius error et expedita inventore
                laboriosam minima, neque quo temporibus tenetur unde vero voluptatibus. Corporis cupiditate iste maiores
                neque tempora!
            </div>
            <div>Aliquid et expedita neque quaerat repellat? At doloremque doloribus, ea, hic magni maxime placeat
                possimus sapiente vitae voluptate voluptatibus voluptatum. Blanditiis dolorem ea enim est excepturi
                laboriosam odio placeat tenetur?
            </div>
            <div>Ad alias assumenda, corporis debitis eaque ex facilis impedit iure nesciunt nihil praesentium
                reiciendis rem. Culpa deleniti ducimus eveniet fugit libero nisi numquam, quis quod sapiente sequi
                tempore vitae voluptates.
            </div>
            <div>Commodi ipsa laborum natus, nostrum possimus quisquam quo reiciendis sunt tenetur? Atque autem dicta
                ducimus inventore ipsum magni modi nam, nihil quis repellat repellendus sed sit tempora vel voluptatem
                voluptatum.
            </div>
            <div>Ad beatae culpa illum, impedit nulla odit possimus praesentium! Adipisci aliquid dolore doloremque est
                modi optio pariatur temporibus vero! Amet animi atque dicta ipsum magni natus neque provident quo rem?
            </div>
            <div>Beatae debitis esse hic molestias nobis odio quas quisquam saepe tempora, ut? Cumque earum eos expedita
                fuga hic ipsum sapiente ut. Autem dignissimos excepturi, fuga iste praesentium sequi ullam voluptatem?
            </div>
        </div>
    </div>

@endsection
