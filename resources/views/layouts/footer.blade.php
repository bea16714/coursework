<footer id="footer">
    <div class="container">
        <section class="floor1 row">
            <div class="products col">
                <div class="name"><a href="">Продукты</a></div>

                <div class="item"><a href="">Компьютеры</a></div>
                <div class="item"><a href="">Ноутбуки</a></div>
                <div class="item"><a href="">Аксессуары</a></div>
            </div>
            <div class="buyers col">
                <div class="name"><a href="">Покупателям</a></div>

                <div class="item"><a href="">Спецпредложения</a></div>
                <div class="item"><a href="">Доставка</a></div>
                <div class="item"><a href="">Оплата</a></div>
            </div>
            <div class="contacts col">
                <div class="name"><a href="#">Контакты</a></div>

                <div class="item"><a href="tel:89998887766">8 (999) 888-77-66</a></div>
                <div class="item"><a href="#"> г. пермь, ул. Луначарского 1</a></div>
                <div class="item"><a href="#">WhatsApp</a></div>
                <div class="item"><a href="#">Telegram</a></div>
            </div>
        </section>

        <section class="floor2">
            <div class="social row">

                <div class="col-lg-2"></div>

                <div class="vk centre col">
                    <a href="#">
                        <span style="color: #4a76a8; fill: currentcolor;" data-uk-icon="icon: hp-vk; ratio: 1.5" class="uk-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 20 20">
                                <path d="M20 5c0,-1 0,-1 -1,-1l-2 0c-1,0 -1,0 -1,1 0,0 -1,2 -3,4 0,1 -1,1 -1,1 0,0 0,0 0,-1l0 -4c0,-1 0,-1 -1,-1l-3 0c-1,0 -1,0 -1,1 0,0 1,0 1,2l0 3c0,1 0,1 0,1 -1,0 -3,-3 -4,-6 0,-1 -1,-1 -1,-1l-2 0c-1,0 -1,0 -1,1 0,0 1,3 3,7 2,3 5,4 7,4 2,0 2,0 2,-1l0 -2c0,-1 0,-1 0,-1 1,0 1,1 2,2 2,1 2,2 3,2l2 0c1,0 1,0 1,-1 0,-1 -1,-1 -2,-3 0,0 -1,-1 -1,-1 -1,-1 -1,-1 0,-1 0,0 2,-4 3,-5z"></path>
                            </svg>
                        </span>
                    </a>

                </div>

                <div class="tiktok centre col">
                    <a href="#">
                        <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" width="30" height="30">
                            <path fill="#ff004f" d="M58 19.4v9.3l-1.7.1c-4.5 0-8.7-1.7-11.9-4.4v19.8a17.8 17.8 0 01-33 9.6 17.5 17.5 0 0026.4-2.3c2.2-3 3.6-6.7 3.6-10.8V20.8c3.2 2.8 7.3 4.4 11.9 4.4l1.7-.1v-6c.9.2 1.7.3 2.6.3h.4z"></path>
                            <path fill="#ff004f" d="M29 26.3v10.3a8.1 8.1 0 00-9.7 10.7 8.3 8.3 0 01-3.4-6.6 8.1 8.1 0 0110.2-7.9v-6.6h.6l2.3.1zM45.9 12a13.4 13.4 0 01-3.8-6.1h2.4v1.4c.2 1.6.7 3.2 1.4 4.7z"></path>
                            <path fill="#ffffff" d="M55.1 19.2v6l-1.7.1c-4.5 0-8.7-1.7-11.9-4.4v19.8c0 4-1.3 7.8-3.6 10.8a17.7 17.7 0 01-26.4 2.3 17.9 17.9 0 0114.6-27.4V33a8.1 8.1 0 00-10.2 7.9 8 8 0 003.4 6.6 8.1 8.1 0 007.5 5.4c4.4 0 8-3.7 8-8.2V5.9h7.3a13 13 0 003.8 6.1c1.8 3.6 5.2 6.3 9.2 7.2z" style="color: #ffffff;"></path>
                            <path fill="#24f5ee" d="M26.1 22.8v3.4a17.9 17.9 0 00-14.6 27.4A18.1 18.1 0 016 40.5a17.9 17.9 0 0120.1-17.7z"></path>
                            <path fill="#24f5ee" d="M42.1 5.9h-7.3v38.6c0 4.5-3.6 8.2-8 8.2a7.9 7.9 0 01-7.5-5.4c1.3.9 2.9 1.5 4.6 1.5a8 8 0 008-8.1V2h9.7v.2l.1 1.2c0 .8.2 1.7.4 2.5zM55.1 15.5v3.6c-4-.8-7.4-3.5-9.3-7.1a13 13 0 009.3 3.5z"></path>
                        </svg>
                    </a>

                </div>

                <div class="instagram centre col">
                    <a href="#">
                        <span class="instagram-icon uk-icon" data-uk-icon="icon: instagram; ratio: 1.5">
                        <svg width="30" height="30" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.55,1H6.46C3.45,1,1,3.44,1,6.44v7.12c0,3,2.45,5.44,5.46,5.44h7.08c3.02,0,5.46-2.44,5.46-5.44V6.44 C19.01,3.44,16.56,1,13.55,1z M17.5,14c0,1.93-1.57,3.5-3.5,3.5H6c-1.93,0-3.5-1.57-3.5-3.5V6c0-1.93,1.57-3.5,3.5-3.5h8 c1.93,0,3.5,1.57,3.5,3.5V14z"></path>
                            <circle cx="14.87" cy="5.26" r="1.09"></circle>
                            <path d="M10.03,5.45c-2.55,0-4.63,2.06-4.63,4.6c0,2.55,2.07,4.61,4.63,4.61c2.56,0,4.63-2.061,4.63-4.61 C14.65,7.51,12.58,5.45,10.03,5.45L10.03,5.45L10.03,5.45z M10.08,13c-1.66,0-3-1.34-3-2.99c0-1.65,1.34-2.99,3-2.99s3,1.34,3,2.99 C13.08,11.66,11.74,13,10.08,13L10.08,13L10.08,13z"></path>
                        </svg>
                    </span>
                    </a>

                </div>

                <div class="youtube centre col">
                    <a href="#">
                        <span class="youtube-icon uk-icon" data-uk-icon="icon: youtube; ratio: 1.5">
                            <svg width="30" height="30" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15,4.1c1,0.1,2.3,0,3,0.8c0.8,0.8,0.9,2.1,0.9,3.1C19,9.2,19,10.9,19,12c-0.1,1.1,0,2.4-0.5,3.4c-0.5,1.1-1.4,1.5-2.5,1.6 c-1.2,0.1-8.6,0.1-11,0c-1.1-0.1-2.4-0.1-3.2-1c-0.7-0.8-0.7-2-0.8-3C1,11.8,1,10.1,1,8.9c0-1.1,0-2.4,0.5-3.4C2,4.5,3,4.3,4.1,4.2 C5.3,4.1,12.6,4,15,4.1z M8,7.5v6l5.5-3L8,7.5z"></path>
                            </svg>
                        </span>
                    </a>
                </div>

                <div class="col-lg-2"></div>

            </div>
        </section>

    </div>

    <section class="floor3">
        <div class="container centre">
            <div class="copyright">© 2021-2022 EDEVICE. Все права не защищены.</div>
            <div class="trademark">"EDEVICE" - не является товарным знаком, зарегистрированным в России.</div>
            <div class="links">
                <a href="#">Copyright</a>
                <div class="separator"> | </div>
                <a href="#">Конфиденциальность</a>
                <div class="separator"> | </div>
                <a href="#">Публичная оферта</a>
                <div class="separator"> | </div>
                <a href="#">Карта сайта</a>
            </div>
        </div>
    </section>

</footer>
