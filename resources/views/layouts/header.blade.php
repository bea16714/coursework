<header>
    <nav class="container">
        <div class="navbar-left">
            <a href="/coursework/public/"><img src="https://hyperpc.ru/templates/hyperpc/images/logos/hyperpc-logo-white-name-only-stencil.svg" alt="logo"></a>
            <ul class="navigate">
                <li><a href="computers">Модели</a></li>
                <li><a href="buyers">Покупателям</a></li>
                <li><a href="owners">Владельцам</a></li>
                <li><a href="about">О нас</a></li>
            </ul>
        </div>
        <div class="navbar-right">
            <div class="geo">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor-fill" viewBox="0 0 16 16">
                        <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103z"/>
                    </svg>
                    <div class="city">Пермь</div>
                </a>
            </div>
            <div class="phones"><a href="tel:+78007776655">8(800)777-66-55</a></div>
            <div class="search">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                </a>
            </div>
            <div class="basket" id="basket">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-bag-check" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                    </svg>
            </div>
            <button class="navbar-toggle" type="button" data-uk-toggle="target: #hp-offcanvas-menu" aria-label="Меню" aria-expanded="false">
                <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <rect y="9" width="20" height="2"></rect>
                    <rect y="3" width="20" height="2"></rect>
                    <rect y="15" width="20" height="2"></rect>
                </svg>
            </button>
        </div>
    </nav>
    <div class="container">
        <div class="dropdown row none" id="dropdown">
            <div class="dropdown-text">
                <div class="dropdown-items" id="dropdownItems">
                {{--          подгружается из кук с помощью js, файл cookies.js          --}}
                </div>
                <div class="dropdown-button text-uppercase"><a class="dropdown-button-a" href="/coursework/public/basket">Перейти в корзину</a></div>
                <div class="dropdown-acc"><a href="user">Войти</a></div>
            </div>
        </div>
    </div>

</header>
<script src="/coursework/resources/js/basket.js"></script>
