@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="page_in_progress container text-center">
        <img class="work-img" src="/coursework/public/img/work.png" alt="work">
        <div class="work-text text-center text-white">Страница в разработке</div>
        <a class="work-link" href="/coursework/public/">На главную</a>
    </div>

@endsection
