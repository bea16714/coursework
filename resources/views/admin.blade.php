@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <h1 class="text-uppercase text-white text-center">Админ-панель</h1>

    <div class="container">
        <div class="orders">
            <h2 class="text-white text-center">Заказы</h2>
            @foreach($orders as $el)
                <div class="orders-img"><img src="/coursework/public/img/notebooks/{{ strtolower(str_replace(" ", '', $el->name_device)) }}.jpg" alt="{{ strtolower(str_replace(" ", '', $el->name_device)) }}"></div>
                <div class="orders-name-device text-white">{{ $el->name_device }}</div>
                <div class="orders-name-device text-white">Номер пользователя: {{ $el->id_user }}</div>
            @endforeach
        </div>
    </div>

@endsection
