@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="notebooks container">
        <h2 class="text-center text-white notebooks-h2">Какой мощный игровой ПК подходит вам?</h2>

        <div class="assortment row">
            <div class="col-xl"></div>

            @foreach($desktop as $el)
                <div class="item col-xl-3 col-6">
                    <div class="top">
                        <a href="pc/{{ strtolower(str_replace(" ", '', $el->name)) }}">
                            <img src="img/notebooks/{{  $el->img }}" alt="">
                            <h3 class="text-white text-center name">{{  $el->name }}</h3>
                            <div class="price text-center text-white">{{  $el->price }}</div>
                        </a>
                        <div class="buy row">
                            <div class="button-buy col">
                                <a class="buy-notebook" href="notebooks/{{ strtolower(str_replace(" ", '', $el->name)) }}#buy">купить</a>
                            </div>

                            <a class="col text-white details-notebook" href="notebooks/{{ strtolower(str_replace(" ", '', $el->name)) }}">
                                Подробнее
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </a>
                        </div>

                    </div>

                    <hr class="margin-medium">

                    <div class="specifications">
                        <div class="item-spec min-height-120">
                            <div class="units text-center text-white">{{  $el->customization }}</div>
                            <div class="caption text-center">Кастомизация</div>
                        </div>

                        <div class="item-spec min-height-120">
                            <div class="units text-center text-white">{{  $el->cooling }}</div>
                            <div class="caption text-center">Охлаждение</div>
                        </div>

                        <div class="item-spec min-height-120">
                            <div class="units text-center text-white">{{  $el->storage }}</div>
                            <div class="caption text-center">Хранилище</div>
                        </div>

                        <div class="item-spec min-height-120">
                            <div class="units text-center text-white">{{  $el->guarantee }}</div>
                            <div class="caption text-center">Гарантия</div>
                        </div>
                    </div>

                    <hr class="margin-medium">

                </div>
            @endforeach

            <div class="col-xl"></div>
        </div>
    </div>

@endsection
