@extends('layout')

@section('main_content')

<section class="slider">
    <div class="slider">
        <div class="slider__wrapper">
            <div class="slider__items">

                @foreach($slider as $el)
                    <div class="slider__item" style="background-image: url('..//..//public/img/slider/desktop/{{ $el->img }}'); max-width: 2560px;">
                        <div class="text container">
                            <span class="big_text">{{ $el->big_text }}</span>
                            <span class="small_text">{{ $el->small_text }}</span>
                            <span><a class="detail" href="#">Подробнее</a></span>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

        <a class="slider__control slider__control_prev" href="#" role="button" data-slide="prev"></a>
        <a class="slider__control slider__control_next" href="#" role="button" data-slide="next"></a>
    </div>
</section>

<section class="catalog indent">
    <h1>Модели E-Device</h1>
    <div class="model container">
        <div class="row">
            <div class="notebook col-lg-3 col-6">
                <a href="notebooks">
                    <img src="img/catalog/notebook.jpg" alt="notebook">
                    <h2 class="title">Мобильные игровые</h2>
                    <div class="text">Мощные и ультратонкие игровые ноутбуки</div>
                    <div class="price">от 100 000 ₽</div>
                    <a class="detailSm" href="notebooks">Подробнее</a>
                </a>
            </div>
            <div class="notebook col-lg-3 col-6">
                <a href="pc">
                    <img src="img/catalog/powerful.jpg" alt="notebook">
                    <h2 class="title">Мощные игровые</h2>
                    <div class="text">Самые мощные игровые компьютеры EDEVICE</div>
                    <div class="price">от 200 000 ₽</div>
                    <a class="detailSm" href="#">Подробнее</a>
                </a>
            </div>
            <div class="notebook col-lg-3 col-6">
                <a href="pc">
                    <img src="img/catalog/base.jpg" alt="notebook">
                    <h2 class="title">Базовые игровые</h2>
                    <div class="text">Доступные игровые компьютеры для дома и учебы</div>
                    <div class="price">от 300 000 ₽</div>
                    <a class="detailSm" href="#">Подробнее</a>
                </a>
            </div>
            <div class="notebook col-lg-3 col-6">
                <a href="pc">
                    <img src="img/catalog/worker.jpg" alt="notebook">
                    <h2 class="title">Рабочие станции</h2>
                    <div class="text">Профессиональные решения для работы и творчества</div>
                    <div class="price">от 400 000 ₽</div>
                    <a class="detailSm" href="#">Подробнее</a>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="select indent">
    <h1>Подобрать компьютер</h1>

    <div class="container">
        <div class="row">
            <div class="by-parameters col">
                <button name="parameters" id="by-parameter" class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"/>
                    </svg>
                    <span class="text">По параметрам</span>
                </button>
            </div>

            <div class="for-games col">
                <button id="for-games" class="enable">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-controller" viewBox="0 0 16 16">
                        <path d="M11.5 6.027a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm-1.5 1.5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm2.5-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm-1.5 1.5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm-6.5-3h1v1h1v1h-1v1h-1v-1h-1v-1h1v-1z"/>
                        <path d="M3.051 3.26a.5.5 0 0 1 .354-.613l1.932-.518a.5.5 0 0 1 .62.39c.655-.079 1.35-.117 2.043-.117.72 0 1.443.041 2.12.126a.5.5 0 0 1 .622-.399l1.932.518a.5.5 0 0 1 .306.729c.14.09.266.19.373.297.408.408.78 1.05 1.095 1.772.32.733.599 1.591.805 2.466.206.875.34 1.78.364 2.606.024.816-.059 1.602-.328 2.21a1.42 1.42 0 0 1-1.445.83c-.636-.067-1.115-.394-1.513-.773-.245-.232-.496-.526-.739-.808-.126-.148-.25-.292-.368-.423-.728-.804-1.597-1.527-3.224-1.527-1.627 0-2.496.723-3.224 1.527-.119.131-.242.275-.368.423-.243.282-.494.575-.739.808-.398.38-.877.706-1.513.773a1.42 1.42 0 0 1-1.445-.83c-.27-.608-.352-1.395-.329-2.21.024-.826.16-1.73.365-2.606.206-.875.486-1.733.805-2.466.315-.722.687-1.364 1.094-1.772a2.34 2.34 0 0 1 .433-.335.504.504 0 0 1-.028-.079zm2.036.412c-.877.185-1.469.443-1.733.708-.276.276-.587.783-.885 1.465a13.748 13.748 0 0 0-.748 2.295 12.351 12.351 0 0 0-.339 2.406c-.022.755.062 1.368.243 1.776a.42.42 0 0 0 .426.24c.327-.034.61-.199.929-.502.212-.202.4-.423.615-.674.133-.156.276-.323.44-.504C4.861 9.969 5.978 9.027 8 9.027s3.139.942 3.965 1.855c.164.181.307.348.44.504.214.251.403.472.615.674.318.303.601.468.929.503a.42.42 0 0 0 .426-.241c.18-.408.265-1.02.243-1.776a12.354 12.354 0 0 0-.339-2.406 13.753 13.753 0 0 0-.748-2.295c-.298-.682-.61-1.19-.885-1.465-.264-.265-.856-.523-1.733-.708-.85-.179-1.877-.27-2.913-.27-1.036 0-2.063.091-2.913.27z"/>
                    </svg>
                    <span class="text">Для игр</span>
                </button>
            </div>

            <div class="for-work col">
                <button id="for-work" class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-hdd-rack" viewBox="0 0 16 16">
                        <path d="M4.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zM3 4.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm2 7a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm-2.5.5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                        <path d="M2 2a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h1v2H2a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-1a2 2 0 0 0-2-2h-1V7h1a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H2zm13 2v1a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1zm0 7v1a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1zm-3-4v2H4V7h8z"/>
                    </svg>
                    <span class="text">Для работы</span>
                </button>
            </div>
        </div>
        <div class="down">
            <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
            </svg>
        </div>
    </div>

    <div class="parameters none" id="parameters">
        <h2>Выберите параметры</h2>

        <div class="container">
            <div class="row">
                <div class="price col">
                    <button id="price">
                        <span class="text">По цене</span>
                    </button>
                </div>

                <div class="gpu col">
                    <button id="gpu">
                        <span class="text">По видеокарте</span>
                    </button >
                </div>

                <div class="cpu col">
                    <button id="cpu">
                        <span class="text">По процессору</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="resolution" id="resolution">
        <h2>Выберите разрешение монитора</h2>

        <div class="container">
            <div class="row">
                <div class="price col">
                    <button id="fullhd">
                        <span class="text">FullHD (1920x1080)</span>
                    </button>
                </div>

                <div class="gpu col">
                    <button id="k2">
                        <span class="text">2K (2560x1440)</span>
                    </button>
                </div>

                <div class="cpu col">
                    <button id="k4">
                        <span class="text">4K (3840x2160)</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="work none" id="work">
        <h2>Выберите свою сферу</h2>

        <div class="container">
            <div class="row">
                <div class="price col-lg-4 col-6 m">
                    <button id="modeling">
                        <span class="text">3D Моделирование</span>
                    </button>
                </div>

                <div class="gpu col-lg-4 col-6 m">
                    <button id="videoediting">
                        <span class="text">Видеомонтаж</span>
                    </button>
                </div>

                <div class="cpu col-lg-4 col-6 m">
                    <button id="visualization">
                        <span class="text">Визуализация в 3D</span>
                    </button>
                </div>

                <div class="gpu col-lg-4 col-6 m lower">
                    <button id="design">
                        <span class="text">Фото и графический дизайн</span>
                    </button>
                </div>

                <div class="gpu col-lg-4 col-6 m last lower">
                    <button id="streaming">
                        <span class="text">Стриминг</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="down none" id="down">
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
        </svg>
    </div>

    <div class="work none" id="price2">
        <h2>Выберите цену</h2>

        <div class="container">
            <div class="row">
                <div class="price col-lg-3 col-6 m">
                    <button id="p100">
                        <span class="text">100 000 ₽</span>
                    </button>
                </div>

                <div class="gpu col-lg-3 col-6 m">
                    <button id="p200">
                        <span class="text">200 000 ₽</span>
                    </button>
                </div>

                <div class="cpu col-lg-3 col-6 m">
                    <button id="p300">
                        <span class="text">300 000 ₽</span>
                    </button>
                </div>

                <div class="gpu col-lg-3 col-6 m">
                    <button id="p400">
                        <span class="text">400 000 ₽</span>
                    </button>
                </div>

            </div>
        </div>
    </div>

    <div class="work none" id="gpu2">
        <h2>Выберите видеокарту</h2>

        <div class="container">
            <div class="row">
                <div class="price col-lg-3 col-6 m">
                    <button id="gpu6">
                        <span class="text">GEFORCE RTX 3060</span>
                    </button>
                </div>

                <div class="gpu col-lg-3 col-6 m">
                    <button id="gpu7">
                        <span class="text">GEFORCE RTX 3070</span>
                    </button>
                </div>

                <div class="cpu col-lg-3 col-6 m">
                    <button id="gpu8">
                        <span class="text">GEFORCE RTX 3080</span>
                    </button>
                </div>

                <div class="gpu col-lg-3 col-6 m">
                    <button id="gpu9">
                        <span class="text">GEFORCE RTX 3090</span>
                    </button>
                </div>

            </div>
        </div>
    </div>

    <div class="work none" id="cpu2">
        <h2>Выберите видеокарту</h2>

        <div class="container">
            <div class="row">
                <div class="price col-lg-4 col-6 m">
                    <button id="cpui5">
                        <span class="text">Intel Core i5</span>
                    </button>
                </div>

                <div class="price col-lg-4 col-6 m">
                    <button id="cpui7">
                        <span class="text">Intel Core i7</span>
                    </button>
                </div>

                <div class="price col-lg-4 col-6 m">
                    <button id="cpui9">
                        <span class="text">Intel Core i9</span>
                    </button>
                </div>

                <div class="price col-lg-4 col-6 m">
                    <button id="cpur5">
                        <span class="text">AMD Ryzen 5</span>
                    </button>
                </div>

                <div class="price col-lg-4 col-6 m">
                    <button id="cpur7">
                        <span class="text">AMD Ryzen 7</span>
                    </button>
                </div>

                <div class="price col-lg-4 col-6 m">
                    <button id="cpur9">
                        <span class="text">AMD Ryzen 9</span>
                    </button>
                </div>

            </div>
        </div>
    </div>

    <div class="down none" id="down2">
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
        </svg>
    </div>
{{--    --}}

</section>

<section class="feedback container">
    <h2>Отзывы наших клиентов</h2>

    <div class="row feedbackBg">
        <div class="balls indent30 col-lg col-6">99%</div>
        <div class="yandex indent30 col-lg col-6">
            <div class="heading">yandex.ru</div>
            <div class="description">9 лет на Яндекс.Маркете</div>
        </div>
        <div class="star indent30 col-lg col-6">
            <div class="heading">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 20 20" data-svg="star">
                    <polygon fill="#ffb300" stroke="#ffb300" stroke-width="1.01" points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 20 20" data-svg="star">
                    <polygon fill="#ffb300" stroke="#ffb300" stroke-width="1.01" points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 20 20" data-svg="star">
                    <polygon fill="#ffb300" stroke="#ffb300" stroke-width="1.01" points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 20 20" data-svg="star">
                    <polygon fill="#ffb300" stroke="#ffb300" stroke-width="1.01" points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 20 20" data-svg="star">
                    <polygon fill="#ffb300" stroke="#ffb300" stroke-width="1.01" points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"/>
                </svg>
            </div>
            <div class="description">93% покупателей купили бы здесь снова</div>
        </div>
        <div class="number indent30 col-lg col-6">
            <div class="heading">1000</div>
            <div class="description">Положительных оценок за все время работы</div>
        </div>
        <div class="rating indent30 col-lg col-6">
            <div class="heading">4.9</div>
            <div class="description">Общий рейтинг магазина за последние 2 месяца</div>
        </div>
    </div>
</section>

@endsection




