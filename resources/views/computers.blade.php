@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="notebooks container">
        <h1 class="text-center text-white notebooks-h1">Игровые ноутбуки</h1>
        <p class="text-center notebooks-text">Самые мощные игровые ноутбуки с полноценной десктопной графикой NVIDIA GeForce RTX 30. Лучшая в классе производительность, лучшие игровые возможности. Испытай настоящее совершенство для игр прямо сейчас</p>
        <h2 class="text-center text-white notebooks-h2">Какой ноутбук HYPERPC PLAY подходит вам?</h2>

        <div class="assortment row">

            @if(count($notebooks) != 0)

                @for($i=0; $i!=count($notebooks); $i++)
                <div class="item col-xl-3 col-6">
                    <div class="top">
                        <a href="/coursework/public/notebooks/{{ strtolower(str_replace(" ", '', $notebooks[$i]['name'])) }}">
                            <img src="/coursework/public/img/notebooks/{{  $notebooks[$i]['img'] }}" alt="">
                            <h3 class="text-white text-center name">{{   $notebooks[$i]['name'] }}</h3>
                            <div class="price text-center text-white">{{   $notebooks[$i]['price'] }}</div>
                        </a>
                        <div class="buy row">
                            <div class="button-buy col text-center">
                                <a class="buy-notebook" href="/coursework/public/notebooks/{{ strtolower(str_replace(" ", '',  $notebooks[$i]['name'])) }}">подробнее</a>
                            </div>
                        </div>
                    </div>

                    <hr class="margin-medium">

                </div>
                @endfor

                @else
                <div class="item col-xl-3 col-6">
                    <div class="top">
                        <div class="buy row">
                            <div class="button-buy col text-center">
                                Туть пусто( <a class="buy-notebook" href="/coursework/public/notebooks/">На главную</a>
                            </div>
                        </div>
                    </div>

                    <hr class="margin-medium">

                </div>
            @endif

        </div>
    </div>

@endsection
