{{--@extends('layouts.app')--}}

{{--@section('content')--}}

{{--@endsection--}}

@extends('layout')

@section('main_content')

<div class="indentHeader"></div>

<script src="{{ asset('js/app.js') }}" defer></script>


<section class="main-content-user container">
    <div class="overhead container text-white">
        <h3>Hi, {{ Auth::user()->name }}</h3>

        <div class="esc" aria-labelledby="navbarDropdown">
            <a class="dropdown-item text-white" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                Выйти
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>

    <div class="address text-white">
        <h4>Ваш адрес: {{ $user->address }}</h4>
        <div class="change-address">
            <a class="btn btn-dark" href="user/address">Изменить адрес</a>
        </div>

    </div>

    <div class="orders">
        <h2 class="text-white text-center">Ваши заказы</h2>
        @foreach($orders as $el)
            <div class="orders-img"><img src="/coursework/public/img/notebooks/{{ strtolower(str_replace(" ", '', $el->name_device)) }}.jpg" alt="{{ strtolower(str_replace(" ", '', $el->name_device)) }}"></div>
            <div class="orders-name-device text-white">{{ $el->name_device }}</div>
        @endforeach
    </div>


</section>



@endsection
