@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <section class="details background-color-black">
        <div class="details-top position-relative height-100">
            <div class="details-top-bg position-cover background-cover" style="background-image: url('../..//..//public/img/notebooks/{{ strtolower(str_replace(" ", '', $details->name)) }}/{{  $details->img1 }}')"></div>
            <h1 class="details-name position-relative text-white text-center">{{  $details->name }}</h1>
            <h2 class="details-tagline position-relative text-center colorful-text">{{  $details->tagline }}</h2>
        </div>

        <div class="details-description container">
            <h2 class="details-description-h1 text-center colorful-text details-h1">{{  $details->description_h1 }}</h2>
            <div class="details-description-text text-center text-color-999 details-text">М{{  $details->description_text }}</div>
        </div>

        <div class="details-margin"></div>

        <div class="details-buy bg-color-28">
            <div class="details-margin-for-bg container">
                <span class="buy-text text-white">
                    <div class="buy-first-str">Станьте владельцем</div>
                    <div class="buy-second-str text-uppercase">{{  $details->name }}</div>
                </span>
                <span class="buy-button-size">
                    <button class="buy-button">Купить</button>
                </span>
            </div>
        </div>

        <div class="details-margin"></div>

        <div class="details-design container">
            <div class="details-topic text-center text-white text-uppercase">ДИЗАЙН</div>
            <div class="details-h1 colorful-text text-center text-uppercase">{{  $details->design_h1 }}</div>
            <div class="details-text text-center">{{  $details->design_description }}</div>
            <div class="specifications row">
                <div class="col-lg"></div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">Длина</div>
                    <div class="specifications-units text-white">{{  $details->length }}</div>
                </div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">Ширина</div>
                    <div class="specifications-units text-white">{{  $details->width }}</div>
                </div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">Толщина</div>
                    <div class="specifications-units text-white">{{  $details->thickness }}</div>
                </div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">Вес</div>
                    <div class="specifications-units text-white">{{  $details->weight }}</div>
                </div>
                <div class="col-lg"></div>
            </div>
        </div>

        <div class="details-bd-img background-cover height-90 text-center" style="background-image: url('../..//..//public/img/notebooks/{{ strtolower(str_replace(" ", '', $details->name)) }}/{{  $details->design_img }}')">
            <canvas width="1000" height="820">&nbsp;</canvas>
        </div>

        <div class="details-margin-100"></div>

        <div class="details-design container">
            <div class="details-topic text-center text-white text-uppercase">Производительность</div>
            <div class="details-h1 colorful-text text-center text-uppercase">{{  $details->performance_h1 }}</div>
            <div class="details-text text-center">{{  $details->performance_description }}</div>
        </div>

        <div class="details-bd-img background-cover height-90 text-center" style="background-image: url('../..//..//public/img/notebooks/{{ strtolower(str_replace(" ", '', $details->name)) }}/{{  $details->performance_img }}')">
            <canvas width="1000" height="820">&nbsp;</canvas>
        </div>

        <div class="specifications container">
            <div class="row">
                <div class="col-lg"></div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">PUBG</div>
                    <div class="specifications-units text-white">{{  $details->performance_pubg }}</div>
                </div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">CoD: Warzone</div>
                    <div class="specifications-units text-white">{{  $details->performance_warzone }}</div>
                </div>
                <div class="specifications-length col col-lg text-center">
                    <div class="specifications-caption">CS:GO</div>
                    <div class="specifications-units text-white">{{  $details->performance_cs }}</div>
                </div>
                <div class="col-lg"></div>
            </div>
            <div class="details-padding-bottom-50"></div>
        </div>

        <div class="details-margin"></div>
    </section>

    <section class="background-color-gray card-buy-center details-padding-bottom-50" id="buy">
        <div class="card-buy">
            <div class="card-buy-img"><img id="card_img" src="../../public/img/notebooks/{{ strtolower(str_replace(" ", '', $details->name)) }}.jpg" class="card-buy-width" alt="123"></div>
            <div class="padding-left-15">
                <h3 class="card-name text-white card-margin" id="buy-name">{{  $details->name }}</h3>
                <div class="card-price text-white card-margin">Цена {{  $details->price }}</div>
                <button class="card-button text-white card-margin" id="in_garbage">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-cart" viewBox="0 0 20 20">
                        <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </svg>
                    <span class="card-button-text">В корзину</span>
                </button>

                <hr class="card-hr">
                <div class="card-platform-caption">CPU</div>
                <div class="card-platform-name">{{  $details->cpu }}</div>
                <hr class="card-hr">
                <div class="card-platform-caption">GPU</div>
                <div class="card-platform-name">{{  $details->gpu }}</div>
                <hr class="card-hr">
                <div class="card-platform-caption">Разрешение дисплея</div>
                <div class="card-platform-name">2{{  $details->resolution }}</div>
            </div>
        </div>
    </section>

    <script src="/coursework/resources/js/buy.js"></script>

@endsection
