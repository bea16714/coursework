<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-Device</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/css/simple-adaptive-slider.min.css">

    <link rel="stylesheet" href="/coursework/resources/css/app.css">

    <script src="../resources/js/simple-adaptive-slider.js"></script>
    <script src="../resources/js/sliderSettings.js"></script>
</head>
<body>

@extends('layouts/footer')

@yield('main_content')

@extends('layouts/header')

<script src="/coursework/resources/js/main.js"></script>
<script src="/coursework/resources/js/cookies.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="/coursework/resources/js/jquery.cookies.js"></script>

</body>
</html>
