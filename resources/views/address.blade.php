@extends('layout')

@section('main_content')

    <div class="indentHeader"></div>

    <div class="container">
        <form action="address/change_address" method="post">
            @csrf
            <input type="text" class="form-control" id="address" name="address" required>
            <button type="submit" class="btn btn-dark" id="address_sub" name="address_sub">Изменить адрес</button>
        </form>
    </div>

@endsection
