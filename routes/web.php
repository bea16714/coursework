<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'welcome']);

Route::get('/notebooks', [MainController::class, 'notebooks']);

Route::get('/notebooks/{name}', [MainController::class, 'notebooks']);

Route::get('/pc/', [MainController::class, 'pc']);

Route::get('/pc/{name}', [MainController::class, 'pc']);

Route::get('/computers', [MainController::class, 'computers']);

Route::get('/computers/{tags}/{value}', [MainController::class, 'computers']);

Route::get('/buyers', [MainController::class, 'buyers']);

Route::get('/owners', [MainController::class, 'owners']);

Route::get('/about', [MainController::class, 'about']);

Route::get('/page_in_progress', [MainController::class, 'page_in_progress']);

Route::get('/basket', [MainController::class, 'basket']);

Route::get('/admin', [MainController::class, 'admin']);


Route::get('/login', 'Auth\AuthController@getLogin');

Auth::routes();

Route::get('/user', [App\Http\Controllers\HomeController::class, 'index'])->name('user');

Route::get('/toorders', [App\Http\Controllers\HomeController::class, 'toorders'])->name('toorders');

Route::get('/user/address', [App\Http\Controllers\HomeController::class, 'address'])->name('address');

Route::post('/user/address/change_address', [App\Http\Controllers\HomeController::class, 'change_address'])->name('change_address');

Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin');
