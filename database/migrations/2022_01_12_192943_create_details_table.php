<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('tagline');
            $table->string('img1');
            $table->string('description_h1');
            $table->text('description_text');
            $table->string('design_h1');
            $table->text('design_description');
            $table->string('length');
            $table->string('width');
            $table->string('thickness');
            $table->string('weight');
            $table->string('design_img');
            $table->string('performance_h1');
            $table->text('performance_description');
            $table->string('performance_img');
            $table->string('performance_pubg');
            $table->string('performance_warzone');
            $table->string('performance_cs');
            $table->string('price');
            $table->string('cpu');
            $table->string('gpu');
            $table->string('resolution');
            $table->string('modeling');
            $table->string('video_editing');
            $table->string('visualization');
            $table->string('design');
            $table->string('streaming');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
